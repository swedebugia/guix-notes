* Merge conflicts

For the newcomer to Guix solving merge conflicts can arise if you e.g. change 
the documentation and somebody commits between your latest pull and the 
time you finish your edit and send a patch.

Resolving merge conflicts is best avoided by communication, but in a distributed
project it is hard to know who edits what when.

Guix.texi is the file most likely to have merge conflicts with.

** Handling a conflict

Example conflict reported by git after doing "git pull":

 : Auto-merging <file-name>
 : CONFLICT (content): Merge conflict in <file-name>
 : Automatic merge failed; fix conflicts and then commit the result.

In Guix no graphical merge tools have been packaged yet as of this writing.

This leaves you with only diff and manually handling the conflict. 
Set up diff and git with:

: $ git config merge.conflictstyle diff3

*** Method 1 (before commit)

You changed the working tree. Now running git pull gives you a conflict like
above

First mark the conflicting files with

 : $ git mergetool

Edit files and observe that they now have lines like these

: <<<<<<< HEAD
: <line>
: =======
: <line>
: >>>>>>> <branch>

Edit until you are satisfied. Then merge the pulled origin master into 
your master

 : $ git merge

Now commit your changes

 : $ git commit

Done!

*** Method 2 (after commit)

Copy the file you modified out of the tree, if you committed then save 
your commit-msg to a file out of tree too and then do:

: $ git reset --hard origin 

Check with "git log" that everything is fine.

Copy your file back into the tree and follow Method 1 above.

See documentation with 
: $ git help reset
or here: https://git-scm.com/docs/git-reset