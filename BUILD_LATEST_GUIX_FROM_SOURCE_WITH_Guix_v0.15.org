* Building Guix from source (using the latest release of Guix v0.15)
Based on INSTALL.org by Pjotr Prins

** Introduction

The easiest way to build guix from source is with first making sure you 
have a suficiently recent guix installed.

You will need the git tree to package new software and work
on reproducible builds.

** Fetch repository with git

Use one of https://savannah.gnu.org/git/?group=guix and clone with
sub modules:

: git clone --recurse git://git.savannah.gnu.org/guix.git

when updating

: git pull --recurse-submodules git-URI

** A note on bootstrap from source

Bootstrapping from source, after checking out the git Guix source tree
can be surprisingly tricky because of the build dependencies. 

For building from source it is particularly important not to *mix*
Guix and native dependencies. Also make sure you are using the proper
localstatedir.

This is my recommended route for building from source. It is a great
route because it uses Guix packages isolated from the rest of the
system using a Guix environment.

You can re-build and re-install Guix using a system that already runs
Guix. 

** Install Guix or GuixSD

Follow the manual. I recommend installing GuixSD in a VM on your favorite 
distribution as a start. XXX

** Install the build dependencies and build tools using Guix:

Pull version 0.15 and (optionally) reconfigure

#+begin_src sh   :lang bash
guix pull --commit=1f44934fb6e2cefccbecd4fa347025349fa9ff76

guix system reconfigure config.scm
#+end_src

After the 0.15 release guile-gcrypt was added to Guix and made a build
dependency. 

Download the definition (install wget if needed):

#+begin_src sh   :lang bash
wget https://gitlab.com/swedebugia/guix-notes/raw/master/guile-gcrypt.scm
#+end_src

Inspect the file with to ensure it does not contain malicious code.

And create the build environment using the pulled guix with

#+begin_src sh   :lang bash
~/.config/guix/current/bin/guix environment -C guix --ad-hoc help2man git \
  strace pkg-config less binutils coreutils grep guile guile-git gcc \
  guile-json po4a guile-sqlite3 --no-grafts -l guile-gcrypt.scm
#+end_src

Tip: once you have built guix successfully you can also use the
"./pre-inst-env" prefix to create the environment.

Use the --no-grafts switch if you have built packages that way before to avoid
triggering a full rebuild.

** Now build it

cd into the source directory if needed and run:

#+begin_src sh   :lang bash
gcc --version
guile --version
rm -rf autom4te.cache/ # to be sure
make clean
./bootstrap
./configure --localstatedir=/var
make clean    # to be really sure
make clean-go # to be even surer
time make
time make check
#+end_src sh   :lang bash

Once you have done it you should be able to run

: ./pre-inst-env guix --version

Now you can start hacking guix. Good luck!

** Troubleshooting

In case of problems, on the mailing list it was suggested to actually
clean everything, git clone a new repository as a test.  Or, try this
(warning: this will remove ALL untracked files and undo ALL changes):

#+begin_src sh   :lang bash
    make distclean
    git clean -dfx
    git reset --hard
#+end_src

the only other thing I can think to clean is your Guile ccache
(although I don't think it's likely the cause), which I believe you
can safely do with the following command:

:  rm -rf ~/.cache/guile/ccache

Finally, especially if you do not use containers (-C switch) the
safest route is by using guix environment after starting a clean shell
(note environment does not clutter up your main profile because you
get a temporary one!):

#+begin_src sh   :lang bash
screen -S guix-build # I tend to build in screen
env -i /bin/bash --login --noprofile --norc
#+end_src